$(document).ready(function(){
    $('.table').DataTable({
        "scrollX": false
    });
    $('.exp-date, .date-picker').datepicker();
    $('.time-picker').timepicker('setTime', '');
    $.fn.editable.defaults.mode = 'inline';
    $('.make-fav').click(function () {
        event.preventDefault();
        $(this).addClass('isFav');
    })
	$.fn.editable.defaults.mode = 'inline';
    $('.d-item-click').editable({});
    //Adding class small when the btn is visible in the HTML
    $('.editable').click(function(){
    	if ($('.editable-buttons').length) {
    	$('.editable-buttons .btn').addClass('btn-sm');
    	};
    })
    $('.caption span').editable({
    	type: 'text',
	    placement: 'top',
	    title: 'Enter public name',
	    toggle:'manual'
    });
    $('.caption button').click(function (event) {
    	event.stopPropagation();
    	$('.caption span').editable('toggle');
    	if ($('.editable-buttons').length) {
    		$('.editable-buttons .btn').addClass('btn-sm');
    	};
    	$(this).hide();
    	$('.editable-buttons .btn').on('click' , function () {
    		$('.caption button').show();
    	});
    });
    function rearrange() {
        $('.por-body tr').each(function() {
            var x = $(this).index();
            $(this).find('.num').text(x + 1);
            // $(this).find('.num').text(x1);
            // console.log($(this).index());
        });
    }
    $('.rearrange-btn').on('click', function(event) {
        event.preventDefault();
        $('.arrange').removeClass('hidden');
        $('.pagination').hide();
        $('.por-body').sortable({
            update: function() {
                rearrange();
            }
        });
    });
    $('.mix-grid').sortable();
    $('.mix').last().removeClass('ui-sortable-handle');
    $('.radio-list input').on('change', function(event) {
        event.preventDefault();
        if ($('#optionsRadios4').attr('checked')) {
            $('.url-video').addClass('hidden');
            $('.mix-grid').removeClass('hidden');
        } else {
            $('.url-video').removeClass('hidden');
            $('.mix-grid').addClass('hidden');
        }
    });
    $('.handle-expand').on('click', function(event) {
        event.preventDefault();
        $(this).closest('.dd-item').siblings().find('.add-panel').slideUp('slow');
        $(this).closest('.dd-item').find('.add-panel').slideToggle('slow');
    });
    var updateOutput = function(e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    $('#nestable').nestable({
            group: 1
        })
        .on('change', updateOutput);

    updateOutput($('#nestable').data('output', $('#nestable-output')));
        updateOutput($('#nestable2').data('output', $('#nestable2-output')));
        $('#nestable-menu').on('click', function(e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

    $('#url').preview({
        key: 'f74cbc4ea5fa4512a41c6632f1ec636b'
    })

    $('.url-form').on('submit', function() {
        $(this).addInputs($('#url').data('preview'));
        return true;
    });    
});